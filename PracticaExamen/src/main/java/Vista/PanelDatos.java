package Vista;

import controlador.Controlador;



public class PanelDatos extends javax.swing.JPanel {
    
    public PanelDatos() {
        initComponents();
    }
    
    public void escuchar(Controlador controlador){
        btnBuscar.addActionListener(controlador);
    }
    
    public void limpiar(){
        txtCedula.setText("");
        txtNombre.setText("");
        txtAltura.setText("");
        txtPeso.setText("");
    }

    public String getTxtCedula() {
        return txtCedula.getText().trim();
    }

    public String getTxtNombre() {
        return txtNombre.getText().trim();
    }

    public double getTxtAltura() {
        return Double.parseDouble(txtAltura.getText());
    }

    public double getTxtPeso() {
        return Double.parseDouble(txtPeso.getText());
    }
    

    public void setTxtCedula(String txtCedula) {
        this.txtCedula.setText(txtCedula);
    }
    
    public void setTxtNombre(String txtNombre) {
        this.txtNombre.setText(txtNombre);
    }

    public void setTxtAltura(double txtAltura) {
        this.txtAltura.setText(Double.toString(txtAltura));
    }
    
    public void setTxtPeso(double txtPeso) {
        this.txtPeso.setText(Double.toString(txtPeso));
    }
    
    
       
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        etiRegistroPacientes = new javax.swing.JLabel();
        etiCedula = new javax.swing.JLabel();
        etiNombre = new javax.swing.JLabel();
        txtCedula = new javax.swing.JTextField();
        txtNombre = new javax.swing.JTextField();
        txtAltura = new javax.swing.JTextField();
        etiAltura = new javax.swing.JLabel();
        btnBuscar = new javax.swing.JButton();
        etiPeso = new javax.swing.JLabel();
        txtPeso = new javax.swing.JTextField();

        etiRegistroPacientes.setFont(new java.awt.Font("Montserrat SemiBold", 0, 44)); // NOI18N
        etiRegistroPacientes.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        etiRegistroPacientes.setText("Registro Pacientes");

        etiCedula.setFont(new java.awt.Font("Montserrat Light", 1, 22)); // NOI18N
        etiCedula.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        etiCedula.setText("Cedula:");

        etiNombre.setFont(new java.awt.Font("Montserrat Light", 1, 22)); // NOI18N
        etiNombre.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        etiNombre.setText("Nombre:");

        txtCedula.setFont(new java.awt.Font("Montserrat Light", 0, 16)); // NOI18N
        txtCedula.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtCedula.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCedulaActionPerformed(evt);
            }
        });

        txtNombre.setFont(new java.awt.Font("Montserrat Light", 0, 16)); // NOI18N
        txtNombre.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNombreActionPerformed(evt);
            }
        });

        txtAltura.setFont(new java.awt.Font("Montserrat Light", 0, 16)); // NOI18N
        txtAltura.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtAltura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAlturaActionPerformed(evt);
            }
        });

        etiAltura.setFont(new java.awt.Font("Montserrat Light", 1, 22)); // NOI18N
        etiAltura.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        etiAltura.setText("Altura:");

        btnBuscar.setFont(new java.awt.Font("Montserrat Light", 1, 16)); // NOI18N
        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        etiPeso.setFont(new java.awt.Font("Montserrat Light", 1, 22)); // NOI18N
        etiPeso.setText("Peso:");

        txtPeso.setFont(new java.awt.Font("Montserrat Light", 0, 16)); // NOI18N
        txtPeso.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtPeso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPesoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(etiCedula)
                    .addComponent(etiNombre)
                    .addComponent(etiAltura)
                    .addComponent(etiPeso))
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtPeso, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtCedula)
                    .addComponent(txtNombre, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtAltura))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnBuscar)
                .addGap(22, 22, 22))
            .addGroup(layout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addComponent(etiRegistroPacientes)
                .addContainerGap(49, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(etiRegistroPacientes)
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(etiCedula)
                    .addComponent(txtCedula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscar))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(etiNombre)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(etiAltura, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtAltura))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(etiPeso, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtPeso))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txtCedulaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCedulaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCedulaActionPerformed

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void txtNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNombreActionPerformed

    private void txtAlturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAlturaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtAlturaActionPerformed

    private void txtPesoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPesoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPesoActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JLabel etiAltura;
    private javax.swing.JLabel etiCedula;
    private javax.swing.JLabel etiNombre;
    private javax.swing.JLabel etiPeso;
    private javax.swing.JLabel etiRegistroPacientes;
    private javax.swing.JTextField txtAltura;
    private javax.swing.JTextField txtCedula;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtPeso;
    // End of variables declaration//GEN-END:variables
}
