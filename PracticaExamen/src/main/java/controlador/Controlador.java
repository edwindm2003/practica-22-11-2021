package controlador;
import Vista.GUIPractica;
import Vista.PanelBotones;
import Vista.PanelDatos;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import modelo.Paciente;
import modelo.RegistroPaciente;

public class Controlador implements ActionListener{
    private PanelDatos panelDatos;
    private RegistroPaciente registroPaciente;
    private Paciente paciente;
    private GUIPractica guiPractica;
    private PanelBotones panelBotones;
    
    public Controlador(GUIPractica guiPractica, PanelDatos panelDatos, PanelBotones panelBotones){
        registroPaciente = new RegistroPaciente();
        this.panelDatos = panelDatos;
        this.guiPractica = guiPractica;
        this.panelBotones = panelBotones;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        String valorLeido = e.getActionCommand();
	switch(valorLeido){
            case "Guardar":
                String nombre = panelDatos.getTxtNombre();
                String cedula = panelDatos.getTxtCedula();
                double altura = panelDatos.getTxtAltura();
                double peso = panelDatos.getTxtPeso();
                paciente = new Paciente(cedula, nombre, altura, peso);
                registroPaciente.agregar(paciente); 
                panelDatos.limpiar();
            break;
			
            case "Calcular IMC":
              nombre = paciente.getNombre();
              peso = paciente.getPeso();
              altura = paciente.getAltura();
              registroPaciente.calcularIMC(peso, altura, nombre);
            break;
			
            case "Salir":
                guiPractica.dispose();
            break;
            
            case "Buscar":
                paciente = registroPaciente.buscar(panelDatos.getTxtCedula());
                if(paciente!=null){
                    panelDatos.setTxtCedula(paciente.getCedula());
                    panelDatos.setTxtNombre(paciente.getNombre());
                    panelDatos.setTxtAltura(paciente.getAltura());
                    panelDatos.setTxtPeso(paciente.getPeso());
                }
                else{
                    guiPractica.mostrarMensaje("El paciente no se encuentra registrado");
                }
            break;
        }
    }
}
