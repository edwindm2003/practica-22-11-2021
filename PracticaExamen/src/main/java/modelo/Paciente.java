
package modelo;

public class Paciente {
    private String cedula;
    private String nombre;
    private double altura;
    private double peso;

    public Paciente(String cedula, String nombre, double altura, double peso) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.altura = altura;
        this.peso = peso;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }



    

    @Override
    public String toString() {
        return "\n---------------------------------------------\nCedula: " + cedula + " \nNombre: " + nombre + " \nAltura: " + altura + "\nPeso" +peso;
    }
}
