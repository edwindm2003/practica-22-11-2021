package modelo;

import java.util.ArrayList;
import javax.swing.JOptionPane;

public class RegistroPaciente {
    private ArrayList<Paciente> listaPacientes;
    private Paciente paciente;
    private double calculoIMC;
    private String mensaje;
    
    public RegistroPaciente(){
        listaPacientes = new ArrayList<Paciente>();
    }
    
    public void agregar(Paciente paciente){
        if(listaPacientes.add(paciente)){
            JOptionPane.showMessageDialog(null,"Se agregó el paciente correctamente");
        }
        else{
           JOptionPane.showMessageDialog(null,"No se agragó el paciente");
        }
    }
    
    
    public Paciente buscar(String cedula){
        for(int posicion = 0;posicion<listaPacientes.size();posicion++){
            if(listaPacientes.get(posicion)!=null){
                if(listaPacientes.get(posicion).getCedula().equalsIgnoreCase(cedula)){
                    return listaPacientes.get(posicion);      
                }
            }
        }
        return null;
    }
    
    public String calcularIMC(double peso, double altura, String nombre){
        calculoIMC = (peso)/(altura*altura);
        
        if(calculoIMC<16){
           JOptionPane.showMessageDialog(null,"El paciente "+nombre+" Se encuentra en criterio de ingreso a hospital");
        }
        else{
            if(calculoIMC>=16 && calculoIMC <= 17){
                JOptionPane.showMessageDialog(null,"El paciente "+nombre+" se encuentra en Infrapeso");
            }
            else{
                if(calculoIMC>=17 && calculoIMC <= 18){
                   JOptionPane.showMessageDialog(null,"El paciente "+nombre+" se encuentra en Bajo peso");
                }
                else{
                    if(calculoIMC>=18 && calculoIMC <= 25){
                        JOptionPane.showMessageDialog(null,"El paciente "+nombre+" se encuentra en peso normal");
                    }
                    else{
                        if(calculoIMC>=25 && calculoIMC <= 30){
                            JOptionPane.showMessageDialog(null,"El paciente "+nombre+" se encuentra en Sobre peso (obesidad grado I)");
                        }
                        else{
                            if(calculoIMC>=30 && calculoIMC <= 35){
                               JOptionPane.showMessageDialog(null,"El paciente "+nombre+" se encuentra en sobre peso cronico (obesidad grado II)");
                            }
                            else{
                                if(calculoIMC>=35 && calculoIMC <= 40){
                                    JOptionPane.showMessageDialog(null,"El paciente "+nombre+" se encuentra en obesidad premorbida (obesidad grado III)");
                                }
                                else{
                                    if(calculoIMC>=40){
                                       JOptionPane.showMessageDialog(null,"El paciente "+nombre+" se encuentra en obesidad morbida (obesidad grado IV)");
                                    }
                                    else{
            
                                    } 
                                } 
                            }              
                        }            
                    }             
                }            
            }
        }
        return "";
        
    }
}
